# Week8-miniproj

> Zilin Xu  
> zx112  

# Project Demo

this is the result after running `cargo test`

![](/截屏2024-03-31%2018.54.33.png)

# Steps

## init

run `cargo new xxxx`

edit the `.toml` file by adding 

```toml
[dependencies]

clap = "3.0"
```

## implementation

under `/src`, edit `main.rs` and `lib.rs`

in this time, I implement a simple addiction tool by using command line

## test with command line

run `cargo run -- <integer1> <integer2>` you will get

```bash
➜  week8-miniproj git:(main) ✗ cargo run -- 1 2              
    Finished dev [unoptimized + debuginfo] target(s) in 0.04s
     Running `target/debug/week8-miniproj 1 2`
The sum is: 3
```

## create unit test

create a new directory called `tests` and create `/tests/test.rs`

create unit tests and run `cargo test` 

you will see the result in above picture