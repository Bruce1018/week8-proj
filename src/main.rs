use std::env;
use week8_miniproj::add;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        eprintln!("Usage: {} <integer1> <integer2>", args[0]);
        std::process::exit(1);
    }

    let num1 = args[1].parse::<i32>().expect("Error parsing first number");
    let num2 = args[2].parse::<i32>().expect("Error parsing second number");

    let result = add(num1, num2);
    println!("The sum is: {}", result);
}
