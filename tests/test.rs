// This tells Rust to bring in the library from your package under test.
extern crate week8_miniproj;

// Import the `add` function for use in tests.
use week8_miniproj::add;

#[test]
fn test_addition_positives() {
    assert_eq!(add(2, 3), 5);
}

#[test]
fn test_addition_negatives() {
    assert_eq!(add(-2, -3), -5);
}
